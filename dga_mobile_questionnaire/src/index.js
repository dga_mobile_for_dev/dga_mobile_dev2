import React from 'react'
import Box from '@material-ui/core/Box'
import Link from '@material-ui/core/Link'
import Radio from '@material-ui/core/Radio'
import RadioGroup from '@material-ui/core/RadioGroup'
import FormControlLabel from '@material-ui/core/FormControlLabel'
import FormControl from '@material-ui/core/FormControl'
import { createMuiTheme, ThemeProvider } from '@material-ui/core/styles'
import { green, orange,blue } from '@material-ui/core/colors'

const outerTheme = createMuiTheme({
  palette: {
    secondary: {
      main: "#1769aa"
    }
  }
})

const innerTheme = createMuiTheme({
  palette: {
    secondary: {
      main: green[500]
    }
  }
})
const theme = createMuiTheme({
  palette: {
    primary: blue,
    secondary: green
  },
  status: {
    danger: 'orange'
  }
})

const defaultProps = {
  m: 1,
  border: 1
}

export const QuestionNareComponent = () => {
  const preventDefault = (event) => event.preventDefault()
  const [value, setValue] = React.useState('female')

  const handleChange = (event) => {
    setValue(event.target.value)
  }
  return (
    <div style={{ width: '100%' }}>
      <Box
        component='span'
        display='block'
        p={1}
        m={1}
        borderRadius='borderRadius'
        borderColor='grey.500'
        {...defaultProps}
      >
        <Box fontSize={20} fontWeight='fontWeightBold'>
          ร่างพระราชบัญญัติสภาวิสาหกิจขนาดกลางขนาดย่อม พ.ศ. 2563
        </Box>
        <Link href='#' onClick={preventDefault}>
          <Box color='text.secondary' fontSize={16} m={1}>
            โครงการโรงพยาบาลบลาบลา...
          </Box>
        </Link>
        <Box mt={1}>
          {[
            "It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).",
            "It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).",
            "It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like)."
          ].map((res, i) => {
            return (
              <Box
                component='span'
                display='block'
                p={1}
                m={1}
                mt={1}
                borderRadius='borderRadius'
                borderColor='grey.500'
                {...defaultProps}
                key={i}
              >
                <Box>{'คำถามที่ 1' + ' ' + res}</Box>
                <Box mt={1}>
                  <ThemeProvider theme={outerTheme}>
                    <FormControl component='fieldset'>
                      <RadioGroup
                        aria-label='gender'
                        name='gender1'
                        value={value}
                        onChange={handleChange}
                      >
                        <FormControlLabel
                          value='0'
                          control={<Radio />}
                          label='ตัวเลือกที่ 1'
                        />
                        <FormControlLabel
                          value='1'
                          control={<Radio />}
                          label='ตัวเลือกที่ 1'
                        />
                      </RadioGroup>
                    </FormControl>
                  </ThemeProvider>
                </Box>
              </Box>
            )
          })}
        </Box>
      </Box>
    </div>
  )
}
