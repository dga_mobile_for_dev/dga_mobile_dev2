# page01_learning

> Made with create-react-library

[![NPM](https://img.shields.io/npm/v/page01_learning.svg)](https://www.npmjs.com/package/page01_learning) [![JavaScript Style Guide](https://img.shields.io/badge/code_style-standard-brightgreen.svg)](https://standardjs.com)

## Install

```bash
npm install --save page01_learning
```

## Usage

```jsx
import React, { Component } from 'react'

import MyComponent from 'page01_learning'
import 'page01_learning/dist/index.css'

class Example extends Component {
  render() {
    return <MyComponent />
  }
}
```

## License

MIT © [Ratchaphoom](https://github.com/Ratchaphoom)
