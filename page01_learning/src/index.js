import React from 'react'
import { makeStyles } from '@material-ui/core/styles'
import Card from '@material-ui/core/Card'
import CardActionArea from '@material-ui/core/CardActionArea'
import CardActions from '@material-ui/core/CardActions'
import CardContent from '@material-ui/core/CardContent'
import Button from '@material-ui/core/Button'
import Typography from '@material-ui/core/Typography'
import Grid from '@material-ui/core/Grid'
import Box from '@material-ui/core/Box'
const useStyles = makeStyles({
  root: {},
  bullet: {
    display: 'inline-block',
    margin: '0 2px',
    transform: 'scale(0.8)'
  },
  title: {},
  pos: {}
})
export const LearnningComponent = () => {
  const classes = useStyles()
  return (
    <Card
      className={classes.root}
      style={{
        backgroundColor: '#3c86ff'
      }}
    >
      <CardContent>
        <Typography
          className={classes.title}
          gutterBottom
          variant='h5'
          style={{ color: '#ffff' }}
        >
          ระบบกฏหมายกลางคืออะไร?
        </Typography>
        <Typography variant='h5' component='h2'></Typography>
        <Typography variant='body2' component='p'>
          <Grid container spacing={3}>
            <Grid item xs={9} style={{ color: '#ffff' }}>
              Lorem Ipsum is simply dummy text of the printing and typesetting
              industry. Lorem Ipsum has been the industry's standard dummy text
              ever since the 1500s, when an unknown printer took a galley of
              type and scrambled it to make a type specimen book.
            </Grid>
            <Grid item xs={3} display='flex' alignItems='flex-bottom'>
              <Grid
                container
                direction='row'
                justify='flex-end'
                alignItems='flex-end'
              >
                <Grid>
                  <Button variant='contained'>เรียนรู้เพิ่มเติม</Button>
                </Grid>
              </Grid>
            </Grid>
          </Grid>
        </Typography>
      </CardContent>
      <CardActions></CardActions>
    </Card>
  )
}

{
  /* <Button size='small'>เรีนรู้เพิ่มเติม</Button> */
}
