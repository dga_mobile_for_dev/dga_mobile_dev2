import React,{useState,useEffect} from 'react'
import { makeStyles, withStyles } from '@material-ui/core/styles'
import Card from '@material-ui/core/Card'
import CardContent from '@material-ui/core/CardContent'
import Typography from '@material-ui/core/Typography'
import Grid from '@material-ui/core/Grid'
import Box from '@material-ui/core/Box'
import postdata from './exampledata/postsdata'
import BarChartIcon from '@material-ui/icons/BarChart'
import { useRoutes,A } from 'hookrouter'
import {ListeningDetailComponent} from 'dga_mobile_listenningdetail'
import { ListeningPrincipleDetailComponent } from 'dga_mobile_listeningprincipaldetail'

// Progress //
import LinearProgress from '@material-ui/core/LinearProgress'
// Progress Style //
const BorderLinearProgress = withStyles((theme) => ({
  root: {
    height: 10,
    borderRadius: 5
  },
  colorPrimary: {
    backgroundColor:
      theme.palette.grey[theme.palette.type === 'light' ? 200 : 700]
  },
  bar: {
    borderRadius: 5,
    backgroundColor: '#1a90ff'
  }
}))(LinearProgress)

// Inspired by the former Facebook spinners.
const useStylesFacebook = makeStyles((theme) => ({
  root: {
    position: 'relative'
  },
  bottom: {
    color: theme.palette.grey[theme.palette.type === 'light' ? 200 : 700]
  },
  top: {
    color: '#1a90ff',
    animationDuration: '550ms',
    position: 'absolute',
    left: 0
  },
  circle: {
    strokeLinecap: 'round'
  }
}))



const useStyles = makeStyles({})

const routes = {
  '/listeningDetail/:id': ({ id }) => <ListeningDetailComponent id={'/listeningDetail/'+id} />
}

export const ListeningListComponent = ({ linkname }) => {
  const classes = useStyles()
  const routeResult = useRoutes(routes)
  const pathString = location.pathname
  const [hide,setHide] = useState("")
  useEffect(()=>{
    setHide(location.pathname)
  })
  const listitem = pathString.includes('listeningDetail') != true
    ? postdata.map((res, i) => {
        return (
          <Box pb={2} style={{ cursor: 'pointer' }}>
            <Grid spacing={2}>
              <A
                href={`listeningDetail/${i}`}
                style={{ textDecoration: 'none' }}
                onClick={() => {
                  setHide(`listeningDetail/${i}`)
                }}
              >
                <Card className={classes.root} key={i}>
                  <CardContent>
                    <Typography className={classes.title} gutterBottom>
                      <Box fontSize={16} fontWeight='fontWeightBold'>
                        {res.title}
                      </Box>
                    </Typography>
                    <Typography className={classes.pos}>
                      <Box fontSize={18} fontWeight='fontWeightBold'>
                        ครั้งที่ {res.id}{' '}
                      </Box>
                    </Typography>

                    <Typography variant='body2' component='p'>
                      <Grid
                        container
                        direction='row'
                        justify='flex-end'
                        alignItems='flex-end'
                      >
                        <Grid xs={6}>
                          <Box pt={2}>
                            <Grid container spacing={3}>
                              <Grid xs={6}>
                                <Box pl={2}>
                                  <p>14 พค. 63</p>
                                </Box>
                              </Grid>
                              <Grid xs={6}>
                                <Box align='right' pr={2}>
                                  <p>30 วัน</p>
                                </Box>
                              </Grid>
                            </Grid>
                            <Grid>
                              <BorderLinearProgress
                                variant='determinate'
                                value={50}
                              />
                            </Grid>
                          </Box>
                        </Grid>
                        <Grid xs={6}>
                          <Grid
                            container
                            direction='row'
                            justify='flex-end'
                            alignItems='flex-end'
                          >
                            <BarChartIcon /> 239
                            <BarChartIcon /> 32
                          </Grid>
                        </Grid>
                      </Grid>
                    </Typography>
                  </CardContent>
                </Card>
              </A>
            </Grid>
          </Box>
        )
      })
    : <div>

    </div>
  return (
    <div>
      <Box pb={2} pt={2} pl={2}>
        {!hide && (
          <Box
            style={{ color: '#2196f3' }}
            fontSize={20}
            fontWeight='fontWeightBold'
          >
            การรับฟังทั้งหมด (ตามที่อยากจะให้ non-login เห็น)
          </Box>
        )}
        {listitem}
        {hide.includes('listeningDetail').toString() === 'true' && (
          <ListeningDetailComponent />
        )}
      </Box>
    </div>
  )
}

