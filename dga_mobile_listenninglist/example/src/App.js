import React from 'react'

import { ListeningListComponent } from 'dga_mobile_listenninglist'
import 'dga_mobile_listenninglist/dist/index.css'

const App = () => {
  return (
    <ListeningListComponent linkname='listeningDetail' />
  )
}

export default App
