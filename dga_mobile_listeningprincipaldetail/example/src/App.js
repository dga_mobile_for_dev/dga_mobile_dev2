import React from 'react'

import { ListeningPrincipleDetailComponent } from 'dga_mobile_listeningprincipaldetail'
import 'dga_mobile_listeningprincipaldetail/dist/index.css'

const App = () => {
  return (
    <ListeningPrincipleDetailComponent text='Create React Library Example 😄' />
  )
}

export default App
