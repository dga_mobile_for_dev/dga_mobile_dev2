import React from 'react'
import Box from '@material-ui/core/Box'
import Link from '@material-ui/core/Link'
import Grid from '@material-ui/core/Grid'
import Button from '@material-ui/core/Button'
import StarIcon from '@material-ui/icons/Star'
import ShareIcon from '@material-ui/icons/Share'
import { makeStyles } from '@material-ui/core/styles'
import Chip from '@material-ui/core/Chip'
import AppBar from '@material-ui/core/AppBar'
import Toolbar from '@material-ui/core/Toolbar'
import { FormOutlined, RightCircleOutlined } from '@ant-design/icons'

const defaultProps = {
  bgcolor: 'background.secondary',
  borderColor: 'grey.500',
  m: 1,
  border: 1
}

const imagesProps = {
  bgcolor: 'grey.500',
  borderColor: 'text.primary',
  m: 1,
  border: 1,
  style: { width: '15rem', height: '15rem' }
}

const videoProps = {
  bgcolor: 'grey.500',
  borderColor: 'text.primary',
  m: 1,
  border: 1,
  style: { width: '50rem', height: '50rem' }
}

const useStyles = makeStyles((theme) => ({
  appBar: {
    top: 'auto',
    bottom: 0,
    border: 1
  }
}))

export const ListeningPrincipleDetailComponent = ({ text }) => {
  const preventDefault = (event) => event.preventDefault()
  const classes = useStyles()
  const handleDelete = () => {
    console.info('You clicked the delete icon.')
  }

  return (
    <div>
      <div style={{ width: '100%' }}>
        <Box
          component='span'
          display='block'
          p={1}
          m={1}
          borderRadius='borderRadius'
          {...defaultProps}
        >
          <Link href='#' onClick={preventDefault} color='inherit'>
            <Box color='grey.500' fontSize={12}>
              {
                'โครงการ บลาบลาบลา > ร่างพระราชบัญญัติสภาวิสาหกิจขนาดกลาง ขนาดย่อย และขนาดย่อยใน พศ....'
              }
            </Box>
          </Link>
          <Box mt={1}>
            <Grid container spacing={3}>
              <Grid item xs={4}>
                <Grid
                  container
                  direction='row'
                  justify='flex-start'
                  alignItems='center'
                >
                  <Box fontWeight='fontWeightBold' mt={2}>
                    {' '}
                    หลักการ{' '}
                  </Box>
                </Grid>
              </Grid>
              <Grid item xs={8}>
                <Grid
                  container
                  direction='row'
                  justify='flex-end'
                  alignItems='center'
                >
                  <Grid>
                    {' '}
                    <Button
                      variant='outlined'
                      size='medium'
                      color='primary'
                      className={classes.margin}
                    >
                      <ShareIcon />
                    </Button>
                  </Grid>
                  <Grid>
                    {' '}
                    <Button
                      variant='outlined'
                      size='medium'
                      color='primary'
                      startIcon={<StarIcon />}
                      className={classes.margin}
                    >
                      ติดตาม
                    </Button>
                  </Grid>
                </Grid>
              </Grid>
            </Grid>
          </Box>
          <Box mt={1} fontWeight='fontWeightBold' fontSize={14}>
            สภาพปัญหา
          </Box>
          <Box mt={1} fontSize={12.5}>
            Lorem Ipsum is simply dummy text of the printing and typesetting
            industry. Lorem Ipsum has been the industry's standard dummy text
            ever since the 1500s, when an unknown printer took a galley of type
            and scrambled it to make a type specimen book. It has survived not
            only five centuries, but also the leap into electronic typesetting,
            remaining essentially unchanged. It was popularised in the 1960s
            with the release of Letraset sheets containing Lorem Ipsum passages,
            and more recently with desktop publishing software like Aldus
            PageMaker including versions of Lorem Ipsum.
          </Box>
          <Box mt={2} fontSize={12.5}>
            Contrary to popular belief, Lorem Ipsum is not simply random text.
            It has roots in a piece of classical Latin literature from 45 BC,
            making it over 2000 years old. Richard McClintock, a Latin professor
            at Hampden-Sydney College in Virginia, looked up one of the more
            obscure Latin words, consectetur, from a Lorem Ipsum passage, and
            going through the cites of the word in classical literature,
            discovered the undoubtable source. Lorem Ipsum comes from sections
            1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes
            of Good and Evil) by Cicero, written in 45 BC. This book is a
            treatise on the theory of ethics, very popular during the
            Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit
            amet..", comes from a line in section 1.10.32.
          </Box>
          <Box mt={1.5}>
            <Link href='#' onClick={preventDefault}>
              ไฟล์แนบ
            </Link>
          </Box>
          <Box mt={2}>
            <Grid
              container
              direction='row'
              justify='flex-start'
              alignItems='center'
            >
              {['images1', 'images2', 'images3'].map((res, i) => {
                return (
                  <Box display='flex' justifyContent='center'>
                    <Box
                      borderRadius='borderRadius'
                      {...imagesProps}
                      align='center'
                      alignItems='center'
                    >
                      {res}
                    </Box>
                  </Box>
                )
              })}
            </Grid>
          </Box>
          <Box mt={2} fontWeight='fontWeightBold' fontSize={14}>
            คำอธิยาย
          </Box>{' '}
          <Box mt={2} fontSize={12.5}>
            "But I must explain to you how all this mistaken idea of denouncing
            pleasure and praising pain was born and I will give you a complete
            account of the system, and expound the actual teachings of the great
            explorer of the truth, the master-builder of human happiness. No one
            rejects, dislikes, or avoids pleasure itself, because it is
            pleasure, but because those who do not know how to pursue pleasure
            rationally encounter consequences that are extremely painful. Nor
            again is there anyone who loves or pursues or desires to obtain pain
            of itself, because it is pain, but because occasionally
            circumstances occur in which toil and pain can procure him some
            great pleasure. To take a trivial example, which of us ever
            undertakes laborious physical exercise, except to obtain some
            advantage from it? But who has any right to find fault with a man
            who chooses to enjoy a pleasure that has no annoying consequences,
            or one who avoids a pain that produces no resultant pleasure?"
          </Box>
          <Box mt={2}>
            <Box display='flex' justifyContent='center'>
              <Box
                borderRadius='borderRadius'
                {...videoProps}
                align='center'
                alignItems='center'
              ></Box>
            </Box>
          </Box>
          <Box mt={2} fontWeight='fontWeightBold' fontSize={14}>
            ผู้ได้รับผลกระทบ
          </Box>
          <Box mt={2} fontSize={12.5}>
            "But I must explain to you how all this mistaken idea of denouncing
            pleasure and praising pain was born and I will give you a complete
            account of the system, and expound the actual teachings of the great
            explorer of the truth, the master-builder of human happiness. No one
            rejects, dislikes, or avoids pleasure itself, because it is
            pleasure, but because those who do not know how to pursue pleasure
            rationally encounter consequences that are extremely painful. Nor
            again is there anyone who loves or pursues or desires to obtain pain
            of itself, because it is pain, but because occasionally
            circumstances occur in which toil and pain can procure him some
            great pleasure. To take a trivial example, which of us ever
            undertakes laborious physical exercise, except to obtain some
            advantage from it? But who has any right to find fault with a man
            who chooses to enjoy a pleasure that has no annoying consequences,
            or one who avoids a pain that produces no resultant pleasure?"
          </Box>
          <Box mt={1}>
            <Link href='#' onClick={preventDefault}>
              ไฟล์แนบ
            </Link>
          </Box>
          <Box mt={1}>
            {['เอกาสารประกอบสภาพปัญหา', 'เอกาสารประกอบสภาพปัญหา'].map(
              (res, i) => {
                return (
                  <Chip
                    variant='outlined'
                    size='small'
                    label={res}
                    onDelete={handleDelete}
                    color='primary'
                    key={i}
                    pl={2}
                  />
                )
              }
            )}
          </Box>
          <Box mt={2} fontWeight='fontWeightBold' fontSize={14}>
            เหตุผล - ความจำเป็น
          </Box>
          <Box mt={2} fontSize={12.5}>
            "But I must explain to you how all this mistaken idea of denouncing
            pleasure and praising pain was born and I will give you a complete
            account of the system, and expound the actual teachings of the great
            explorer of the truth, the master-builder of human happiness. No one
            rejects, dislikes, or avoids pleasure itself, because it is
            pleasure, but because those who do not know how to pursue pleasure
            rationally encounter consequences that are extremely painful. Nor
            again is there anyone who loves or pursues or desires to obtain pain
            of itself, because it is pain, but because occasionally
            circumstances occur in which toil and pain can procure him some
            great pleasure. To take a trivial example, which of us ever
            undertakes laborious physical exercise, except to obtain some
            advantage from it? But who has any right to find fault with a man
            who chooses to enjoy a pleasure that has no annoying consequences,
            or one who avoids a pain that produces no resultant pleasure?"
          </Box>
          <Box mt={1}>
            <Link href='#' onClick={preventDefault}>
              ไฟล์แนบ
            </Link>
          </Box>
          <Box mt={1}>
            {['เอกาสารประกอบสภาพปัญหา', 'เอกาสารประกอบสภาพปัญหา'].map(
              (res, i) => {
                return (
                  <Chip
                    variant='outlined'
                    size='small'
                    label={res}
                    onDelete={handleDelete}
                    color='primary'
                    key={i}
                    pl={2}
                  />
                )
              }
            )}
          </Box>
          <Box mt={25}>
            <AppBar
              position='fixed'
              variant='outlined'
              className={classes.appBar}
              style={{
                backgroundColor: '#ffff',
                borderStyle: 'solid',
                borderColor: 'gray'
              }}
            >
              <Toolbar>
                <Grid
                  container
                  direction='row'
                  justify='center'
                  alignItems='center'
                >
                  <Grid>
                    <Box align='center' color='success.main' fontWeight="fontWeightBold" fontSize={28}>
                      {' '}
                      <FormOutlined style={{paddingRight:"1rem"}}/>
                      เริ่มแสดงความคิดเห็น{' '}
                    </Box>
                  </Grid>
                </Grid>
              </Toolbar>
            </AppBar>
          </Box>
        </Box>
      </div>
    </div>
  )
}
