# test01

> Made with create-react-library

[![NPM](https://img.shields.io/npm/v/test01.svg)](https://www.npmjs.com/package/test01) [![JavaScript Style Guide](https://img.shields.io/badge/code_style-standard-brightgreen.svg)](https://standardjs.com)

## Install

```bash
npm install --save test01
```

## Usage

```jsx
import React, { Component } from 'react'

import MyComponent from 'test01'
import 'test01/dist/index.css'

class Example extends Component {
  render() {
    return <MyComponent />
  }
}
```

## License

MIT © [Ratchaphoom](https://github.com/Ratchaphoom)
