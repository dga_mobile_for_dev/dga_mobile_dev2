import React from 'react'

import { ExampleComponent } from 'test01'
import 'test01/dist/index.css'

const App = () => {
  return <ExampleComponent text="Create React Library Example 😄" />
}

export default App
