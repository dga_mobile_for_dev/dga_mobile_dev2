# dga_mobile_listenningdetail

> Made with create-react-library

[![NPM](https://img.shields.io/npm/v/dga_mobile_listenningdetail.svg)](https://www.npmjs.com/package/dga_mobile_listenningdetail) [![JavaScript Style Guide](https://img.shields.io/badge/code_style-standard-brightgreen.svg)](https://standardjs.com)

## Install

```bash
npm install --save dga_mobile_listenningdetail
```

## Usage

```jsx
import React, { Component } from 'react'

import MyComponent from 'dga_mobile_listenningdetail'
import 'dga_mobile_listenningdetail/dist/index.css'

class Example extends Component {
  render() {
    return <MyComponent />
  }
}
```

## License

MIT © [Ratchaphoom](https://github.com/Ratchaphoom)
