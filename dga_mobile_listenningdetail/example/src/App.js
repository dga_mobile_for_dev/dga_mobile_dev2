import React from 'react'

import { ListeningDetailComponent } from 'dga_mobile_listenningdetail'
import 'dga_mobile_listenningdetail/dist/index.css'

const App = () => {
  return <ListeningDetailComponent detailId={window.location.pathname.toString()} />
}

export default App
