import React, { useState, useEffect } from 'react'
import { makeStyles, withStyles } from '@material-ui/core/styles'
import Box from '@material-ui/core/Box'
import Link from '@material-ui/core/Link'
import Chip from '@material-ui/core/Chip'
import Grid from '@material-ui/core/Grid'
import Button from '@material-ui/core/Button'
import ShareIcon from '@material-ui/icons/Share'
import StarIcon from '@material-ui/icons/Star'
import LinearProgress from '@material-ui/core/LinearProgress'
import Card from '@material-ui/core/Card'
import CardContent from '@material-ui/core/CardContent'
import { FormOutlined, RightCircleOutlined } from '@ant-design/icons'
import { useRoutes, A } from 'hookrouter'
import DescriptionIcon from '@material-ui/icons/Description'
import { ListeningPrincipleDetailComponent } from 'dga_mobile_listeningprincipaldetail'
const BorderLinearProgress = withStyles((theme) => ({
  root: {
    height: 10,
    borderRadius: 5
  },
  colorPrimary: {
    backgroundColor:
      theme.palette.grey[theme.palette.type === 'light' ? 200 : 700]
  },
  bar: {
    borderRadius: 5,
    backgroundColor: '#1a90ff'
  }
}))(LinearProgress)

// Inspired by the former Facebook spinners.

// Link //
const useStyles = makeStyles((theme) => ({
  root: {
    '& > * + *': {
      marginLeft: theme.spacing(2)
    }
  }
}))

// Chip //
const chipStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    justifyContent: 'center',
    flexWrap: 'wrap',
    '& > *': {
      margin: theme.spacing(0.5)
    }
  }
}))

// Button //
const buttonStyles = makeStyles((theme) => ({
  margin: {
    margin: theme.spacing(1)
  },
  extendedIcon: {
    marginRight: theme.spacing(1)
  }
}))

// CardButton //
const cardstyles = makeStyles({})

const defaultProps = {
  bgcolor: 'background.paper',
  borderColor: 'grey.500',
  m: 1,
  border: 1
}

const routes = {
  '/listeningDetail/principaldetail/:id': ({ id }) => (
    <ListeningPrincipleDetailComponent id={id} />
  )
}

export const ListeningDetailComponent = ({ detailId, id }) => {
  const buttonClasses = buttonStyles()
  const cardButtonclasses = cardstyles()
  const routeResult = useRoutes(routes)
  const [hide, setHide] = useState(location.pathname)
  console.log(detailId)
  console.log(id)
  const preventDefault = (event) => event.preventDefault()
  const handleDelete = () => {
    console.info('You clicked the delete icon.')
  }

  useEffect(() => {
    setHide(location.pathname)
  })

  const handleClick = () => {
    console.info('You clicked the Chip.')
  }
  return (
    <div>
      <div style={{ width: '100%' }}>
        {location.pathname.includes('principaldetail') !== true ? (
          <Box
            component='span'
            display='block'
            p={1}
            m={1}
            borderRadius='borderRadius'
            {...defaultProps}
          >
            <Box fontSize='h6.fontSize' m={1}>
              ร่างพระราชบัญญัติสภาวิสาหกิจขนาดกลางขนาดย่อม พ.ศ. 2563
            </Box>
            <Link href='#' onClick={preventDefault}>
              <Box color='text.secondary' fontSize={16} m={1}>
                โครงการโรงพยาบาลบลาบลา...
              </Box>
            </Link>
            <Box pt={2} pl={1}>
              <Grid
                container
                direction='row'
                justify='flex-start'
                alignItems='center'
                spacing={3}
              >
                <Grid item xs={4}>
                  <Grid
                    container
                    direction='row'
                    justify='flex-start'
                    alignItems='center'
                  >
                    <Chip
                      size='small'
                      label={'เปิดรับฟัง ครังที่ 3'}
                      style={{ backgroundColor: '#5eca3a' }}
                    />
                    <Box fontWeight='fontWeightBold' ml={2}>
                      ภายใต้ กลต.
                    </Box>
                  </Grid>
                </Grid>
                <Grid item xs={8}>
                  <Grid
                    container
                    direction='row'
                    justify='flex-end'
                    alignItems='center'
                  >
                    <Button
                      variant='outlined'
                      size='medium'
                      color='primary'
                      className={buttonClasses.margin}
                    >
                      <ShareIcon />
                    </Button>
                    <Button
                      variant='outlined'
                      color='primary'
                      size='large'
                      className={buttonClasses.button}
                      startIcon={<StarIcon />}
                    >
                      ติดตาม
                    </Button>
                  </Grid>
                </Grid>
              </Grid>
            </Box>
            <Box pt={1}>
              <Grid
                container
                direction='row'
                justify='flex-end'
                alignItems='center'
              >
                <Link href='#' onClick={preventDefault}>
                  <Box fontSize={12}> ข้อมูลเหตุการณ์ทั้งหมด</Box>
                </Link>
              </Grid>
            </Box>
            <Box pt={1}>
              <BorderLinearProgress variant='determinate' value={50} />
              <Box pt={1}>
                <Grid
                  container
                  direction='row'
                  justify='space-between'
                  alignItems='center'
                >
                  <Grid>
                    {' '}
                    <Box fontSize={13} fontWeight='fontWeightBold'>
                      14 สค 63
                    </Box>{' '}
                  </Grid>
                  <Grid>
                    {' '}
                    <Box fontSize={13} fontWeight='fontWeightBold'>
                      120 วัน
                    </Box>{' '}
                  </Grid>
                  <Grid>
                    {' '}
                    <Box fontSize={13} fontWeight='fontWeightBold'>
                      14 พย 63
                    </Box>{' '}
                  </Grid>
                </Grid>
              </Box>
            </Box>
            <Box pt={2}>
              <Card
                className={cardButtonclasses.root}
                style={{
                  backgroundColor: '#5eca3a',
                  cursor: 'pointer',
                  maxheight: '90px'
                }}
              >
                <CardContent>
                  <Grid
                    container
                    direction='row'
                    justify='center'
                    alignItems='center'
                  >
                    <Grid>
                      <Box variant='h1' pt={1} style={{ color: 'white' }}>
                        <FormOutlined
                          style={{ fontSize: '22px', paddingRight: '2px' }}
                        />{' '}
                        แสดงความคิดเห็น
                      </Box>
                    </Grid>
                  </Grid>
                </CardContent>
              </Card>
            </Box>
            <Box pt={2}>
              {[
                'ข้อมูลสรุปจากหน่วยงาน',
                'หลักการ',
                'ร่างกฏหมาย',
                'ร่วมสนทนา'
              ].map((res, i) => {
                return (
                  <Box
                    component='span'
                    display='block'
                    p={1}
                    m={1}
                    borderRadius='borderRadius'
                    {...defaultProps}
                    key={i}
                    style={{ cursor: 'pointer' }}
                  >
                    <A
                      href={`/listeningDetail/principaldetail/${i}`}
                      style={{ textDecoration: 'none', color: 'gray' }}
                      onClick={() =>
                        setHide(`/listeningDetail/principaldetail/${i}`)
                      }
                    >
                      <Grid
                        container
                        direction='row'
                        justify='flex-start'
                        alignItems='center'
                        spacing={3}
                      >
                        <Grid item xs={8}>
                          <Grid
                            container
                            direction='row'
                            justify='flex-start'
                            alignItems='center'
                          >
                            <Grid>
                              <DescriptionIcon />
                            </Grid>
                            <Grid>
                              <Box pl={2}> {res}</Box>
                            </Grid>
                          </Grid>
                        </Grid>
                        <Grid item xs={4}>
                          <Grid
                            container
                            direction='row'
                            justify='flex-end'
                            alignItems='center'
                          >
                            <RightCircleOutlined />
                          </Grid>
                        </Grid>
                      </Grid>
                    </A>
                  </Box>
                )
              })}
            </Box>
            <Box pt={2}>
              <Chip
                label='หมวดหมู่ที่ 1'
                onDelete={handleDelete}
                variant='outlined'
              />
              <Box mt={1}>
                <Grid
                  container
                  direction='row'
                  justify='flex-start'
                  alignItems='center'
                >
                  <Grid>
                    {' '}
                    <Chip
                      label='tag# 1'
                      onDelete={handleDelete}
                      variant='outlined'
                      color='primary'
                    />
                  </Grid>
                  <Grid>
                    {' '}
                    <Chip
                      label='tag# 2'
                      onDelete={handleDelete}
                      variant='outlined'
                      color='primary'
                    />
                  </Grid>
                </Grid>
              </Box>
            </Box>
          </Box>
        ) : (
          <div></div>
        )}
        {hide.includes('principaldetail').toString() === 'true' && routeResult}
      </div>
    </div>
  )
}
