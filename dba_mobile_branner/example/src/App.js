import React from 'react'

import { BrannerComponent } from 'dba_mobile_branner'
import 'dba_mobile_branner/dist/index.css'

const App = () => {
  return <BrannerComponent/>
}

export default App
