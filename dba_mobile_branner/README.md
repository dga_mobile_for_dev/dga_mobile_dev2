# dba_mobile_branner

> Made with create-react-library

[![NPM](https://img.shields.io/npm/v/dba_mobile_branner.svg)](https://www.npmjs.com/package/dba_mobile_branner) [![JavaScript Style Guide](https://img.shields.io/badge/code_style-standard-brightgreen.svg)](https://standardjs.com)

## Install

```bash
npm install --save dba_mobile_branner
```

## Usage

```jsx
import React, { Component } from 'react'

import MyComponent from 'dba_mobile_branner'
import 'dba_mobile_branner/dist/index.css'

class Example extends Component {
  render() {
    return <MyComponent />
  }
}
```

## License

MIT © [](https://github.com/)
