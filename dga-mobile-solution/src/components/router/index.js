import React from 'react'
import {BrowserRouter as Router , Switch , Route} from 'react-router-dom'
import LearningandListeningComponent from '../learning_and_listening_list'
import ListeningDetailCompoent from '../listeningDetail';
import QuestionNaresComponent from '../questionnare';

const RouterComponent = () => {
    return (
      <div>
        <Router>
          <Switch>
            <Route exact path="/" component={LearningandListeningComponent} />
            <Route
              path="/listeningDetail"
              component={ListeningDetailCompoent}
            />
            <Route path="/questionnare" component={QuestionNaresComponent} />
          </Switch>
        </Router>
      </div>
    );
}

export default RouterComponent;
