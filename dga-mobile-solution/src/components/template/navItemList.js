import React from 'react'
import { makeStyles } from "@material-ui/core/styles";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemAvatar from "@material-ui/core/ListItemAvatar";
import Avatar from "@material-ui/core/Avatar";
import PersonOutlineIcon from "@material-ui/icons/PersonOutline";
import Box from '@material-ui/core/Box';
import ListItemSecondaryAction from "@material-ui/core/ListItemSecondaryAction";
import Button from "@material-ui/core/Button";
import Paper from "@material-ui/core/Paper";
import DemoMenuItem from "./demodata/DemoMenuItem";
const useStyles = makeStyles((theme) => ({
  root: {
    width: "100%",
    maxWidth: 400,
    backgroundColor: theme.palette.background.paper,
  },
  paper: {
    padding: theme.spacing(1),
    textAlign: "center",
    color: theme.palette.text.secondary,
  },
}));

const defaultProps = {
  bgcolor: "background.paper",
  borderColor: "grey.500",
};

const NavItemList = () => {
    const classes = useStyles();
    return (
      <div>
        <List className={classes.root}>
          <ListItem>
            <ListItemAvatar>
              <Avatar>
                <PersonOutlineIcon style={{ color: "#b2102f" }} />
              </Avatar>
            </ListItemAvatar>
            <Box
              fontWeight="fontWeightBold"
              fontSize={18}
              style={{ color: "#b2102f" }}
            >
              ยังไม่ได้เข้าสู่ระบบ
            </Box>
            <ListItemSecondaryAction>
              <Button
                variant="outlined"
                color="primary"
                style={{ color: "#2196f3" }}
              >
                Login
              </Button>
            </ListItemSecondaryAction>
          </ListItem>
          
          {DemoMenuItem.map((res, i) => {
            return (
              <ListItem key={i}>
                <div style={{ width: "100%" }}>
                  <Box
                    display="flex"
                    p={2}
                    borderRadius="borderRadius"
                    {...defaultProps}
                    boxShadow={3}
                    fontWeight="fontWeightBold"
                  >
                    {res.tittle}
                  </Box>
                </div>
              </ListItem>
            );
          })}
        </List>
      </div>
    );
}

export default NavItemList;
