import IndexTemplate from './components/template';
import { BrannerComponent } from "dba_mobile_branner/dist/index.js";
import Grid from "@material-ui/core/Box";
import RouterComponent from './components/router';
function App() {
  return (
    <div>
      <IndexTemplate />
      <Grid item xs={12} pt={3}>
        <BrannerComponent />
      </Grid>
      <RouterComponent/>
    </div>
  );
}

export default App;
